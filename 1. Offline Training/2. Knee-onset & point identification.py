# -*- coding: utf-8 -*-
"""
Created on Mon Jul  3 19:27:03 2023

@author: Administrator
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Jul  3 17:46:02 2023

@author: Administrator
"""

import pandas as pd
import numpy as np
from scipy import rand
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
# from PyEmd import EEMD
#from PyEMD import EEMD, EMD,CEEMDAN
from sklearn.svm import SVR
from scipy.optimize import differential_evolution
import torch.nn as nn
import torch.utils.data as Data
from torch.autograd import Variable
import torch
from sklearn.preprocessing import MinMaxScaler
import os
import random
import joblib
#maths functions
from numpy import matmul as mm
from numpy.linalg import inv
from scipy.interpolate import UnivariateSpline as spline

import seaborn as sns
os.environ["CUDA_LAUNCH_BLOCKING"]= "1"
np.random.seed(123)
random.seed(123)
torch.manual_seed(123)
torch.cuda.manual_seed(123)
torch.cuda.manual_seed_all(123)
torch.backends.cudnn.benchmark= False
torch.backends.cudnn.deterministic= True
plt.rcParams["font.family"]= "Times New Roman"

#colours 
yellow=np.array([1,.67,.14])
lgrey =np.array([.7,.7,.7])
lblue =np.array([.557, .729,.898])
jade=np.array([0,.66, .436])
blue=np.array([.057,.156,.520])
brown=np.array([.515,.158,.033])
red=np.array([.85,.20,0])
gold=np.array([1,.67,.14])
claret=np.array([.429,.073,.238])
grey=np.array([.585,.612,.675])
black=np.array([0,0,0]) #t

# -*- coding: utf-8 -*-
"""
Created on Sun Nov 08 05:57:16 2020
@author: Johannes
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import matplotlib
#matplotlib.rcParams['font.sans-serif'] = "Times New Roman"
# matplotlib.rcParams['font.family'] = "sans-serif"
matplotlib.rcParams['font.size'] = 18

#maths functions
from numpy import matmul as mm
from numpy.linalg import inv
from scipy.interpolate import UnivariateSpline as spline


def ols(x,y):
    """
    Ordinary least squares fitting
    """
    # add the column of ones as the bias
    X = np.concatenate( ( np.ones((x.shape[0],1)), x ),
                        axis=1)
    # full calculation
    w = mm( inv( mm(X.T,X) ), mm(X.T,y) )
    
    # return w
    return w

# defining the knee point identification function
# function identifies the position of the knee point on the capacity time curve.
# This fucntion will take assumptions about the 
# general shape, but should be applicable to any curves with a flat
#  profile in early life followed by a dramatic loss of health. 
# the process finds meeting point of two linear extrapolations
# from early and late life, then uses an angle bisector to find the 
# knee point on the actual curve.

def bacon_watts_knee_point(x, alpha0, alpha1, alpha2, x1):
    ### from paper Identification and machine learning prediction of knee-point and knee-onset in capacity degradation curves of lithium-ion cells equastion 1.
    return alpha0 + alpha1*(x - x1) + alpha2*(x - x1)*np.tanh((x - x1) / 1e-8)

def double_bacon_watts_model(x, alpha0, alpha1, alpha2, alpha3, x0, x2):
    return alpha0 + alpha1*(x - x0) + alpha2*(x - x0)*np.tanh((x - x0)/1e-8) + alpha3*(x - x2)*np.tanh((x - x2)/1e-8)

# BACON WATTS
from scipy.optimize import minimize
def bacon_watts_knee1(plt,time,capacity,colour):
    plt.plot(time,capacity,color=colour,label= "SOH", linewidth=2)
    # plt.ylim(0.78, 1)
    
    # guarantee the inputs are column vectors
    time = time.reshape((time.shape[0],1))
    capacity = capacity.reshape((capacity.shape[0],1))
    
    def bw_func(x,p):
        r = x-p[3]
        y =   p[0] \
            + p[1]*r \
            + p[2]*r*np.tanh(r/(10**-5))
        return y
    def loss_func(p):
        L = (capacity-bw_func(time,p))**2
        return L.sum()
    p0 = np.array([100,-.01,-.01,400.0])
    M = minimize(loss_func,p0,method='Nelder-Mead')
    
    # plot the bacon watts method with the parameters
    plt.plot(time,bw_func(time,M.x),'--',color="green",label= "Bacon watts fitted line", linewidth=3)
    
    # find the knee point time
    t_knee = M.x[3]
    
    # find the capacity of the knee point
    spl_cap = spline(time,capacity,k=5)
    q_knee = spl_cap(t_knee)
    
    # plot knee point and line used for it
    plt.scatter(t_knee,q_knee,color="red",marker='s',s= 100,label= "bacon_watts knee_point")
    plt.vlines(t_knee, 0, 2, color= grey, linewidth=3, linestyle='--')
    plt.legend(loc= "best", fontsize= 15)
    plt.xlabel("Cycle", fontsize= 15)
    plt.ylabel("capacity", fontsize= 15)
    
    return t_knee, q_knee

def d2qdt2(t,q):
    dqdt = np.diff(q,axis=0) / np.diff(t,axis=0)
    d2q = np.diff(dqdt,axis=0) / np.diff(t[1:].reshape((t.shape[0]-1,1)),axis=0)
    return d2q

# #####Bisector
def knee_point_identification(plt,time,capacity,colour):
    """
    Function identifies the position of the knee point on the capacity
    time curve. This function will take several assumptions about the 
    general shape, but should be applicable to any curves with a flat
    profile in early life followed by a dramatic loss of health.
    
    The process finds the meeting point of two linear extrapolations 
    from early and late life, then uses an angle bisector to find the 
    knee point on the actual curve.
    """
    plt.plot(time,capacity,color=colour,label= "capacity", linewidth=1)
    # plt.ylim(0.79, 1)
    
    # guarantee the inputs are column vectors
    t_scale = time.max()
    time = time.reshape((time.shape[0],1))/time.max()
    capacity = capacity.reshape((capacity.shape[0],1))
    
    # ============== EARLY LIFE LINEAR MODEL ================= #
    # find early life data (assumed first half of points)
    n = np.floor(time.shape[0]/2).astype(int)
    t_early = time[:n,:].reshape((n,1))
    q_early = capacity[:n,:].reshape((n,1))
    
    # early life model
    w_early = ols(t_early,q_early)
    q_early = mm(np.hstack((np.ones(time.shape),time)),w_early)
    
    plt.plot(time*t_scale,q_early,'--',color=red,linewidth=2)
    
    # ============== LATE LIFE LINEAR MODEL ================= #
    # find late life data (assumed last 5 points)
    n = time.shape[0]
    t_late = time[n-50:,:].reshape((50,1))
    q_late = capacity[n-50:,:].reshape((50,1))
    
    # late life model
    w_late = ols(t_late,q_late)
    q_late = mm(np.hstack((np.ones(time.shape),time)),w_late)
    
    plt.plot(time*t_scale,q_late,'--',color=red,linewidth=2)
    
    # ================== ANGLE BISECTOR ===================== #
    # intersection point
    t_int = (w_late[0]-w_early[0])/(w_early[1]-w_late[1])
    q_int = w_early[0] + w_early[1]*t_int
    
    # bisector gradient
    m_b = np.tan( (np.arctan(-1/w_early[1]) + np.arctan(-1/w_late[1]) ) /2 )

    # smoothed spline (needed later)
    spl_cap = spline(time,capacity,k=3)
    t_spl = np.linspace(time.min(),time.max(),1850).reshape((1850,1))
    q_spl = spl_cap(t_spl)
    q_bisector = (m_b*t_spl*t_scale) + q_int - (m_b*t_int*t_scale)
    
    plt.plot(t_spl[650:850]*t_scale,q_bisector[650:850],'--',color=grey)
    
    # ================= FINAL KNEE POINT ==================== #    
    # make sure there is a result, otherwise return inf
    if t_spl[ (q_spl>q_bisector) ].shape[0]>0:
        # knee time
        t_knee = np.max( t_spl[ (q_spl>q_bisector) ] )
        # knee point
        q_knee = spl_cap(t_knee)
    
    else:
        t_knee = np.inf
        q_knee = np.inf
    
    plt.scatter(t_knee*t_scale,q_knee,color="black",marker='s',s=200,label=  "Bisector knee_point")
    plt.vlines(t_knee*t_scale, 0, 2, color= grey, linewidth=2, linestyle='--')
    plt.legend(loc= "best", fontsize= 15)
    plt.xlabel("Cycle", fontsize= 15)
    plt.ylabel("SOH", fontsize= 15)
    
    return t_knee*t_scale, q_knee,  t_spl[650:850]*t_scale,q_bisector[650:850]


def kneedle_identification1(plt,time,capacity,colour):
    plt.plot(time,capacity,color=colour, label= "Capacity", linewidth=1)
    # plt.ylim(0.79, 1)
    
    
    # guarantee the inputs are column vectors
    time = time.reshape((time.shape[0],1))
    capacity = capacity.reshape((capacity.shape[0],1))

    # actual kneedle algorithm from Satopaa
    
    # nomalise the capacity curve
    d_time = time.max()-time.min();
    d_cap = capacity.max()-capacity.min();
    q_norm = (capacity - capacity.min())/d_cap;
    t_norm = (time - time.min())/d_time;
    
    # store 1/sqrt(2) = cos(\theta)
    sqrt2 = .5**.5;
    
    # create new x variable
    x = sqrt2*t_norm - sqrt2*q_norm;
    # rotate to make the bisector the x-axis from 0 to 1;
    y = sqrt2*t_norm + sqrt2*q_norm;
    
    # find maximum deviation
    indx = np.argmax(y)
    
    x_indx = x[indx]; y_indx = y[indx];
    
    t_norm_knee = sqrt2*(x_indx+y_indx);
    q_norm_knee = sqrt2*(-x_indx+y_indx);
    
    t_knee = (t_norm_knee*d_time) + time.min()
    q_knee = (q_norm_knee*d_cap) + capacity.min()
    
    x_axis_x = sqrt2*(x+y_indx);
    x_axis_y = sqrt2*(-x+y_indx);
    x_axis_t = (x_axis_x*d_time) + time.min()
    x_axis_q = (x_axis_y*d_cap) + capacity.min()
    
    # plot an example x-axis for the kneedle method
    plt.plot(x_axis_t[40:]-140,x_axis_q[40:]-5,'--',color=grey)
    
    # plot the 45 degree bisector (this is not automated because I want to 
    # make something that can be understood on Figure 4)
    plt.plot(np.array([262,365]),np.array([86.8,93.53]),'--',color=grey)
    
    # mark the knee point
    plt.scatter(t_knee,q_knee,color="black",marker='s',s=100,label=  "kneedle knee_point")
    #ax.plot(np.array([t_knee,t_knee]),np.array([80,98]),
         #   '--',color=grey)
    # ax.text(-10,85,'peak height from chord',color=grey)
    plt.vlines(t_knee, 0, 2, color= grey, linewidth=2, linestyle='--')
    plt.legend(loc= "best", fontsize= 15)
    plt.xlabel("Cycle", fontsize= 15)
    plt.ylabel("SOH", fontsize= 15)
    return t_knee, q_knee
    
# GPs
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF
def diao_knee1(plt,time,capacity,colour):
    plt.plot(time,capacity,color=colour,label= "Capacity", linewidth=1)
    # plt.ylim(0.79, 1)
    
    # guarantee the inputs are column vectors
    time = time.reshape((time.shape[0],1))
    capacity = capacity.reshape((capacity.shape[0],1))
    
    # ============== EARLY LIFE LINEAR MODEL ================= #
    # find early life data (assumed first half of points)
    n = np.floor(time.shape[0]/2).astype(int)
    t_early = time[:n,:].reshape((n,1))
    q_early = capacity[:n,:].reshape((n,1))
    
    # early life model
    w_early = ols(t_early,q_early)
    q_early = mm(np.hstack((np.ones(time.shape),time)),w_early)
    
    plt.plot(time,q_early,'--',color=red,linewidth=3)
    
    # ============== LATE LIFE LINEAR MODEL ================= #
    # find late life data (assumed last 5 points)
    n = time.shape[0]
    t_late = time[n-50:,:].reshape((50,1))
    q_late = capacity[n-50:,:].reshape((50,1))
    
    kernel = RBF(1e2, (8e1, 1e4))
    gp = GaussianProcessRegressor(kernel=kernel)
    gp.fit(time, capacity)
    t_gpm = np.linspace(time.min(),time.max(),1000).reshape((1000,1))
    q_gpm = gp.predict(t_gpm, return_std=False)
    
    d2q = d2qdt2(t_gpm,q_gpm)
    knee_indx = np.argmin(d2q[500:])
    t_late = t_gpm[knee_indx+495:knee_indx+505]
    q_late = q_gpm[knee_indx+495:knee_indx+505]
    
    
    w_late = ols(t_late,q_late)
    q_late = mm(np.hstack((np.ones(time.shape),time)),w_late)
    
    plt.plot(time,q_late,'--',color=red,linewidth=3)
    
    # ================== KNEE POINT CALC ===================== #
    # intersection point
    t_knee = (w_late[0]-w_early[0])/(w_early[1]-w_late[1])
    
    spl_cap = spline(time,capacity,k=5)
    q_knee = spl_cap(t_knee)
    
    #ax.scatter(t_knee,q_knee,color="black",marker='s')
    plt.scatter(t_knee,q_knee,color="blue",marker='d',s=100,label= "diao et al knee_point")
    plt.vlines(t_knee, 0, 2, color= grey, linewidth=3, linestyle='--')
    plt.legend(loc= "best", fontsize= 15)
    plt.xlabel("Cycle", fontsize= 15)
    plt.ylabel("SOH", fontsize = 15)
    #plt.plot(np.array([t_knee,t_knee]),np.array([80,98]),
            #'--',color=grey)
    
    return t_knee, q_knee, q_early, q_late


if __name__== "__main__":
    device= "cuda" if torch.cuda.is_available() else "cpu"
    colours = [blue, jade, gold, red, black, grey]
    file_path= "Source_FC1_Stack_Voltage.csv"

    with open(file_path, "r") as file:
         df= pd.read_csv(file, index_col= False)
    
         df= df[["Time","Tvoltage"]].dropna()
        
         
         for col in df.columns[1:]:
             df[col]=df[col].map(float)
         plotarr = np.rot90(df.drop(labels='Time',axis=1).values)

         plotarr_scaled = []
         #scale the time series of each variable between 0 and 1:
         scaler = MinMaxScaler(feature_range=(0, 1))
         for row in plotarr:                
             X = row
             Xres = X.reshape(-1, 1)
             scaler.fit(Xres)
             Xtrans = scaler.transform(Xres)  
             rescaled = Xtrans.reshape(1, -1)[0]
             plotarr_scaled.append(rescaled)                  
         plotarr_scaled=np.array(plotarr_scaled)

         cmap = plt.get_cmap('Spectral')
         fig, ax = plt.subplots(figsize=(8, 4))
         ax.grid(False)
         plt.imshow(plotarr_scaled, interpolation='none',cmap=cmap, aspect='auto')
         plt.xlabel("Time", fontsize= 15)
         ax.set_xticks(np.arange(len(df))[::99])
         ax.set_xticklabels(df.iloc[::99, :].Time.values,rotation=90,   size =12)     
         ax.set_yticks(np.arange(0,df.columns[1:].shape[0]))
         ax.set_yticklabels(df.columns[1:], rotation=90,  size = 15)     
         plt.colorbar() 
         plt.tight_layout()
         plt.show()
         # fig.savefig('multivariate_timeseries_8hr.jpg', dpi=300)
         plt.clf() 
         
         # check correlation of features with power consumption
         corr = df.corr(method='pearson')
         SOH_corr = pd.DataFrame(corr['Tvoltage'].sort_values(ascending=False))
         print(SOH_corr)

# # create correlation matrix
#          corr_idx = df.corr().sort_values("Tvoltage", ascending=False).index
#          corr_sorted = df.loc[:, corr_idx]  

#          plt.rcParams["font.family"]= "Times New Roman"
#          plt.figure(figsize = (8,3))
#          sns.set(font_scale=0.90)
#          ax = sns.heatmap(corr_sorted.corr().round(3), annot=True, square=True, linewidths=.75, cmap="coolwarm", fmt = ".2f", annot_kws = {"size": 11})
#          ax.xaxis.tick_bottom()
#          plt.title("Correlation Matrix")
#          plt.show()
    
    ### SOH (state of Health)
    df_Battery_1= df[["Time", "Tvoltage"]].dropna()
    

    ############### knee-point identification ###############
    cycle= np.array(df_Battery_1["Time"])

    x_data= np.arange(1, len(df_Battery_1)+ 1)
    y_data= np.array(df_Battery_1["Tvoltage"])
    
    # plt.scatter(x_data, y_data, s=10,label= "Tvoltage", color= "black")
    # plt.legend(loc= "best", fontsize= 15)
    # plt.xlabel("Time", fontsize= 15)
    # plt.ylabel("Tvoltage", fontsize= 15)
    # #plt.rc('axes', labelsize=10)
    # plt.show()
    
    t = np.array(df['Time']).reshape((df['Time'].shape[0],1))
    q = np.array(df['Tvoltage']).reshape((df['Time'].shape[0],1))
    
    
    # dd = d2qdt2(t,q)
    # plt.figure(num= 1, figsize= (13, 10))
    # plt.plot(x_data[2:], dd, linewidth=3)
    # plt.xlabel("Time", fontsize= 25)
    # plt.ylabel("Amplitude", fontsize= 28)
    # plt.xticks(fontsize=23)
    # plt.yticks(fontsize=23)
    
    # fig, ax = plt.subplots()
    
    # ############### Bacon watts method 1 ##########################
    # # cycle_knee, q_knee=bacon_watts_knee1(plt,t,q,colours[4])
    
    # # print("The optimal knee point for battery 1 locate at #", np.round(cycle_knee), "cycle")
    # # print("The optimal knee point for battery 1 locate at #", q_knee, "Qdischarge")
    
    # ###############diao_knee(plt,time,capacity,colour)##############
    # diao_knee, diao_q_knee, q_early, q_late = diao_knee1(plt,t,q,colours[4])
    # print("The optimal diao_knee_point for battery 1 locate at #", np.round(diao_knee), "Time")
    # print("The optimal diao_knee_point for battery 1 locate at #", diao_q_knee, "capacity")
   
    #   ###############kneedle##################################
    # kneedle_t_knee, kneedle_q_knee = kneedle_identification1(plt,t,q,colours[3])
    # print("The optimal kneedle_knee_point for battery 1 locate at #", np.round(kneedle_t_knee), "Time")
    # print("The optimal kneedle_knee_point for battery 1 locate at #", kneedle_q_knee, "capacity")
   
    # #### Bisector ################
    # knee_t_knee, knee_q_knee, q_early,q_bisector = knee_point_identification(plt,t,q,colours[0])
    # print("The optimal Bisector_knee_point for battery 1 locate at #", np.round(knee_t_knee), "Time")
    # print("The optimal Bisector_knee_point for battery 1 locate at #", knee_q_knee, "capacity")
   
    
   
    
    figure_show= False

    parameter_bound= [1, -1e-4, -1e-4, len(y_data)* 0.7]
    coe, cov= curve_fit(bacon_watts_knee_point, x_data, y_data, parameter_bound)
    opt_alpha0, opt_alpha1, opt_alpha2, opt_x1= coe
    upper_x1, lower_x1= coe[3]+ 1.96* np.diag(cov)[3], coe[3]- 1.96* np.diag(cov)[3]

    ############### knee_point (bacon_watts model) ###############
    knee_point= round(opt_x1)

    bacon_watts_first_segment_curve= bacon_watts_knee_point(x_data, opt_alpha0, opt_alpha1, opt_alpha2, opt_x1)[: round(opt_x1)]
    distance_bacon_watts_degradation= ((bacon_watts_first_segment_curve- y_data[: round(opt_x1)])** 2)

    ############### knee-onset (bacon_watts model) ###############
    knee_onset= np.where(distance_bacon_watts_degradation== np.min(distance_bacon_watts_degradation[-500: ]))[0][0]

    parameter_bound1= [opt_alpha0, opt_alpha1 + opt_alpha2/2, opt_alpha2, opt_alpha2/2, 0.8*opt_x1, 1.1*opt_x1]
    coe1, cov= curve_fit(double_bacon_watts_model, x_data, y_data, parameter_bound1)
    opt_alpha0, opt_alpha1, opt_alpha2, opt_alpha3, opt_x0, opt_x2= coe1
    double_upper_x1, double_lower_x1= coe1[4]+ 1.96* np.diag(cov)[4], coe1[4]- 1.96* np.diag(cov)[4]

    ############### knee-onset (double bacon-watts model) ###############
    knee_onset_double_bacon_watts= round(opt_x0)

    plt.figure(num= 1, figsize= (13, 6))
    plt.plot(x_data, y_data, label= "Smoothed Total Voltage",color= "orange", linewidth=3)
    #plt.plot(x_data, bacon_watts_knee_point(x_data,*coe), label= "Bacon_watts fitted line", linewidth=3)
    plt.scatter(knee_point, y_data[knee_point], label= "knee point", color= "black", s= 100, marker= "d")
    plt.scatter(knee_onset, y_data[knee_onset], label= "Bacon_watts knee-onset", color= "blue", s= 100, marker= "s")
    plt.scatter(knee_onset_double_bacon_watts, y_data[knee_onset_double_bacon_watts], label= "Duble_Bacon_watts(knee-onset)", color= "red", s= 100, marker= "o")
    # plt.scatter(287, y_data[287], label= "kneedle_identification", color= "green", s= 200, marker= "s")
    # plt.vlines(coe[3], 0, 2, color='black', linewidth=2, linestyle='--')
    # plt.vlines(lower_x1, 0, 2, color='red', linewidth=1, linestyle='--')
    # plt.vlines(upper_x1, 0, 2, color='red', linewidth=1, linestyle='--')
    #plt.plot(x_data, double_bacon_watts_model(x_data,*coe1), label= "Duble_Bacon_watts fitted line", linewidth=3)
    # plt.vlines(knee_onset_double_bacon_watts, 0, 2, color='black', linewidth=2)
    # plt.vlines(double_lower_x1, 0, 2, color='red', linewidth=2)
    # plt.vlines(double_upper_x1, 0, 2, color='red', linewidth=2)
    # plt.hlines(y = 0.80, xmin = 0, xmax = x_data.max()+500,color='black', linewidth=2)
    # plt.hlines(y = 0.88, xmin = 0, xmax = x_data.max()+500,color='red', linewidth=2)
    # plt.vlines(double_upper_x1, 0, 2, color='red', linewidth=2)
    plt.legend(loc= "best", fontsize= 10)
    plt.xlabel("Time", fontsize= 12)
    plt.ylabel("Total Voltage", fontsize= 12)
    # plt.ylim(0.75, 1)
    # plt.xlim(-25, x_data.max()+100)
    #plt.grid(alpha=.3)
    plt.show()

    print("---------------------------------------------------------------------")
    print("The optimal knee point for battery 1 locate at #", knee_point, "hours")
    print("The optimal knee onset for battery 1 locate at #", knee_onset, "hours")
    print("The optimal knee onset (double Bacon Watts) for battery 1 locate at #", knee_onset_double_bacon_watts, "hours")

   