import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import tensorflow as tf
import random
seed = 93
np.random.seed(seed)
random.seed(seed)
tf.random.set_seed(seed)
from tensorflow.keras.layers import LSTM, Dense, Bidirectional, Dropout
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from tensorflow.keras.optimizers import Adam
from evaluate_data import *
from tensorflow.keras.layers import *
from tensorflow.keras.models import *
from tensorflow.keras import backend as K

# This is the most basic attention that based on Bahdanau et al(2015)
class BahdanauAttention(Layer):
    
    def __init__(self, return_sequences=True):
        self.return_sequences = return_sequences
        super(BahdanauAttention,self).__init__()
        
    def build(self, input_shape):
        
        self.W=self.add_weight(name="att_weight", shape=(input_shape[-1],1),
                               initializer="normal")
        self.b=self.add_weight(name="att_bias", shape=(input_shape[1],1),
                               initializer="zeros")
        
        super(BahdanauAttention,self).build(input_shape)
        
    def call(self, x):
        
        e = K.tanh(K.dot(x,self.W)+self.b)
        a = K.softmax(e, axis=1)
        output = x*a
        
        if self.return_sequences:
            return output
        
        return K.sum(output, axis=1)
    
    def compute_output_shape(self,input_shape):
        return (input_shape[0],input_shape[-1])

    def get_config(self):
        return super(BahdanauAttention,self).get_config()
    
def fit_preprocessing(train, real_columns, categorical_columns):
    real_scalers =  MinMaxScaler().fit(train[real_columns].values)
    # scaler = MinMaxScaler()
    # real_scalers =   scaler.fit_transform(train[real_columns].values)
    categorical_scalers = {}
    num_classes = []
    for col in categorical_columns:
        srs = train[col].apply(str) 
        categorical_scalers[col] = LabelEncoder().fit(srs.values)
        num_classes.append(srs.nunique())
    return real_scalers, categorical_scalers

battery = pd.read_csv("Source_Model_Input_Data_FC1csv")
battery=battery.dropna()			
# ts=1385; sp=1885; eol=2235	

ts=334
###TV_eol=3.345-3.345*0.04,


# ##battery =battery.loc[(battery['Time'] >=ts)]
###battery= battery.iloc[: np.where(np.array(battery["TV"])<= TV_eol)[0][0], :]

###Optimal model hyperparameters for source model training on stack FC voltage predictions 
n_output= 1
epochs= 590#590#550#275
lookback=10#10#10#10
n_features= 1+ lookback
# neurons_2=134
neurons_1= 134#281#182#381
batch_size= 17#12#11#19
dropout= 0.0098#0.03786#0.00455


data = battery.loc[:,['Time','TV', 'Fuesed_HI']]
data_cycle = pd.DataFrame(data['Time'])
data_cycle_train = data_cycle[ts:]
data_HI = pd.DataFrame(data['Fuesed_HI'])
data_HI_train =data_HI[ts:]
data_SOH = pd.DataFrame(data['TV'])
data_SOH_train = data_SOH[ts:]

#####

cycle_sc = MinMaxScaler()
HI_sc = MinMaxScaler()
TV_sc = MinMaxScaler()

data_cycle_sc = cycle_sc.fit_transform(data_cycle)
data_cycle_sc_df = pd.DataFrame(data=data_cycle_sc, columns=['Time'])
data_cycle_train_sc = cycle_sc.transform(data_cycle_train)
data_cycle_train_sc_df = pd.DataFrame(data=data_cycle_train_sc, columns=['Time'])
data_HI_sc = HI_sc.fit_transform(data_HI)
data_HI_train_sc = HI_sc.transform(data_HI_train)
data_HI_train_sc_df = pd.DataFrame(data=data_HI_train_sc, columns=['Fuesed_HI'])
data_TV_sc = TV_sc.fit_transform(data_SOH)
data_TV_train_sc = TV_sc.transform(data_SOH_train)
data_TV_train_sc_df = pd.DataFrame(data=data_TV_train_sc, columns=['TV'])

# data_input = pd.concat(data_cycle_train_sc_df ,data_HI_train_sc_df)

for i in range(lookback):
    data_TV_train_sc_df ['TV'+str(i+1)] = data_TV_train_sc_df ['TV'].shift(-i-1)

train_window = pd.concat([data_HI_train_sc_df[lookback:].reset_index(drop=True),
                            data_TV_train_sc_df], axis=1,ignore_index=True)

train_data = np.array(train_window)[:-lookback]

X_train = train_data[:,0:-1]
y_train = train_data[:, -1:]

def loss(x,y): # x: predicted value, y:actual
    mse = np.sum((x-y)**2)/len(y)
    rmse = mse ** 0.5
    mae = np.sum(np.absolute(x-y))/len(y)
    mape = np.sum(np.absolute(x-y)/y)/len(y)*100
    return mse,rmse,mae, mape
    # ,mape

def draw(preds,true):
    plt.figure(figsize=(7, 5))
    plt.plot(preds,color='royalblue')
    # plt.plot(preds1,color='green')
    plt.plot(true,color='crimson')
    plt.legend(['prediction','actual'], fontsize=15)
    plt.ylabel('Total Voltage (volt)', fontsize=15)
    plt.xlabel('Time (h)', fontsize=15)
    plt.title('Predicted Vs Actual Voltage plot', fontsize=15)
    plt.show()

def draw2(preds,preds1,true):
    plt.figure(figsize=(7, 5))
    plt.plot(preds,color='royalblue')
    plt.plot(preds1,color='green')
    plt.plot(true,color='crimson')
    plt.legend(['prediction','actual'], fontsize=15)
    plt.axhline(y=0.8, color='rosybrown', linestyle='-')
    plt.ylabel('SOH', fontsize=15)
    plt.xlabel('Cycle', fontsize=15)
    plt.title('Predicted Vs Actual SOH plot for test data', fontsize=15)
    plt.show()

def compile_and_fit(model, X_train, y_train, batch_size, epochs=20):
    model.compile(loss=tf.losses.MeanSquaredError(), optimizer=tf.optimizers.Adam())
    history = model.fit(X_train, y_train, epochs=epochs,
                        # callbacks=[early_stopping],
                        batch_size=batch_size,
                        verbose=0)
    return history


def bilstm_att(neurons_1, neurons_2, dropout,n_output,pre_model=None, freeze=False,verbose=True):
    inputs = tf.keras.Input(shape=(1,n_features))
    b1 = Bidirectional(LSTM(neurons_1, return_sequences=True))(inputs)
    b2 = Bidirectional(LSTM(neurons_2, return_sequences=True, activation='relu'))(b1)
    drop = Dropout(dropout)(b2)
    att = BahdanauAttention(return_sequences=False)(drop)
    outputs =  Dense(n_output)(att)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    if pre_model:
        for i in range(2, len(model.layers) - 1):
            model.layers[i].set_weights(pre_model.layers[i].get_weights())
            if freeze: 
                model.layers[i].trainable = False
        
    model.compile(optimizer=Adam(), loss='mse', metrics=['accuracy'])
    if verbose: print(model.summary())    
    
    return model


# def bilstm(neurons_1, dropout, n_output,pre_model=None,freeze=False,verbose=True):
#     inputs = tf.keras.Input(shape=(1,n_features))
#     b1 = Bidirectional(LSTM(neurons_1,
#                         return_sequences=True))(inputs)
#     # b2 = Bidirectional(LSTM(neurons_2,
#     #                     return_sequences=True, activation='relu'))(b1)
#     drop = Dropout(dropout)(b1)
#    # att = BahdanauAttention(return_sequences=False)(drop)
#     outputs =  Dense(n_output)(drop)
#     model = tf.keras.Model(inputs=inputs, outputs=outputs)
#     if pre_model:
#         for i in range(2, len(model.layers) - 1):
#             model.layers[i].set_weights(pre_model.layers[i].get_weights())
#             if freeze: 
#                 model.layers[i].trainable = False
        
#     model.compile(optimizer=Adam(), loss='mse', metrics=['accuracy'])
#     if verbose: print(model.summary())    
    
#     return model

# def lstm_Att(neurons_1, dropout, n_output,pre_model=None,freeze=False,verbose=True):
#     inputs = tf.keras.Input(shape=(1,n_features))
#     b1 = LSTM(neurons_1,
#                         return_sequences=True)(inputs)
#     # b2 = LSTM(neurons_2,
#     #                     return_sequences=True, activation='relu')(b1)
#     drop = Dropout(dropout)(b1)
#     att = BahdanauAttention(return_sequences=False)(drop)
#     outputs =  Dense(n_output)(att)
#     model = tf.keras.Model(inputs=inputs, outputs=outputs)
#     if pre_model:
#         for i in range(2, len(model.layers) - 1):
#             model.layers[i].set_weights(pre_model.layers[i].get_weights())
#             if freeze: 
#                 model.layers[i].trainable = False
        
#     model.compile(optimizer=Adam(), loss='mse', metrics=['accuracy'])
#     if verbose: print(model.summary())    
    
#     return model

# def lstm(neurons_1, dropout, n_output,pre_model=None,freeze=False,verbose=True):
#     inputs = tf.keras.Input(shape=(1,n_features))
#     b1 = LSTM(neurons_1, return_sequences=True, activation='relu')(inputs)
#     # b2 = LSTM(neurons_2,
#     #                     return_sequences=True, activation='relu')(b1)
#     drop = Dropout(dropout)(b1)
#     # att = BahdanauAttention(return_sequences=False)(drop)
#     outputs =  Dense(n_output)(drop)
#     model = tf.keras.Model(inputs=inputs, outputs=outputs)
#     if pre_model:
#         for i in range(2, len(model.layers) - 1):
#             model.layers[i].set_weights(pre_model.layers[i].get_weights())
#             if freeze: 
#                 model.layers[i].trainable = False
        
#     model.compile(optimizer=Adam(), loss='mse', metrics=['accuracy'])
#     if verbose: print(model.summary())    
    
#     return model

X_roll = X_train.copy()
y_roll = y_train.copy()
X_train = X_train.reshape(X_train.shape[0], 1, X_train.shape[1])

global result
global result1
result = '\nEvaluation.'
result1 = 'Computation Time' 

import time
start = time.time()
tf.random.set_seed(seed)
bilstm_att_model=bilstm_att(neurons_1,neurons_2,dropout, n_output)
bilstm_att_history=compile_and_fit(bilstm_att_model,X_train,y_train, batch_size, epochs=epochs)
end = time.time()
print(round((end-start),2),"seconds")
y_pred_train_bilstm_att = bilstm_att_model.predict(X_train)
# draw(y_pred_train_bilstm_att, y_train)


# ##BiLSTM_TR
# import time
# start1 = time.time()
# tf.random.set_seed(seed)
# bilstm_model=bilstm(neurons_1, dropout, n_output)
# bilstm_att_history=compile_and_fit(bilstm_model,X_train,y_train,batch_size, epochs=epochs)
# end1 = time.time()


# print(round((end1-start1),2),"seconds")
# y_pred_train_bilstm= bilstm_model.predict(X_train)
# y_pred_train_bilstm = np.array(y_pred_train_bilstm).reshape(-1,1)

# # draw(y_pred_train_bilstm, y_train)

# ##LSTM_AM_TR
# import time
# start_lstm = time.time()
# tf.random.set_seed(seed)
# lstm_model=lstm_Att(neurons_1,  dropout, n_output)
# bilstm_att_history=compile_and_fit(lstm_model,X_train,y_train, batch_size, epochs=epochs)
# end_lstm = time.time()


# print(round((end_lstm-start_lstm),2),"seconds")
# y_pred_train_lstm= lstm_model.predict(X_train)
# y_pred_train_lstm = np.array(y_pred_train_lstm).reshape(-1,1)

# ##LSTM_TR
# import time
# start_lstm = time.time()
# tf.random.set_seed(seed)
# lstm_model1=lstm(neurons_1,  dropout, n_output)
# bilstm_att_history=compile_and_fit(lstm_model1,X_train,y_train, batch_size, epochs=epochs)
# end_lstm = time.time()


# print(round((end_lstm-start_lstm),2),"seconds")
# y_pred_train_lstm1= lstm_model1.predict(X_train)
# y_pred_train_lstm1 = np.array(y_pred_train_lstm1).reshape(-1,1)



##transfer learning

##1.BiLSTM_Att_TR

bilstm_att_model.save('bilstm_att_model_FC1')
from tensorflow.keras.models import load_model
pre_model=load_model('bilstm_att_model_FC1')

# ##2. BiLSTM_TL
# bilstm_model.save('bilstm_model_FC1')
# from tensorflow.keras.models import load_model
# pre_model1=load_model('bilstm_model_FC1')

# ##3. LSTM_ATT TL
# lstm_model.save('lstm_model_FC1')
# from tensorflow.keras.models import load_model
# pre_model_lstm=load_model('lstm_model_FC1')

# ##4. LSTM_TL
# lstm_model1.save('lstm_model_FC11')
# from tensorflow.keras.models import load_model
# pre_model_lstm1=load_model('lstm_model_FC11')


battery1 = pd.read_csv('C:/Users/USER/Desktop/MODEL_FIRST PAPER/Target model_input_data_FC2.csv')
battery1=battery1.dropna()
ts=334#

TV_eol=3.33-3.33*0.06  

battery1 =battery1.loc[(battery1['Time'] >=ts)]
battery1= battery1.iloc[: np.where(np.array(battery1["TV"])<= TV_eol)[0][0], :]
Tl=int(len(battery1)*0.80) ##eol=2235

###Optimal model hyperparameters for tetsing Target model on FC stack voltage predicitons
# n_output=1
# epochs=590
# lookback=10
# n_features=1+lookback
# neurons_1=134
# # neurons_2=134
# batch_size=17
# dropout=0.0098    


data = battery1.loc[:,['Time','TV', 'Fuesed_HI']]
data_cycle = pd.DataFrame(data['Time'])
data_cycle_train = data_cycle[:Tl]
data_HI = pd.DataFrame(data['Fuesed_HI'])
data_HI_train =data_HI[:Tl]
data_SOH = pd.DataFrame(data['TV'])
data_SOH_train = data_SOH[:Tl]

  ##### test data or unknown data##

data_cycle_test = data_cycle[Tl:]
data_HI_test =data_HI[Tl:]
data_SOH_test = data_SOH[Tl:]



Y_train = np.array(data_SOH[:Tl+lookback])
Y_test = np.array(data_SOH[Tl+lookback:])

#####

cycle_sc = MinMaxScaler()
HI_sc = MinMaxScaler()
TV_sc = MinMaxScaler()


data_cycle_sc = cycle_sc.fit_transform(data_cycle)
data_cycle_sc_df = pd.DataFrame(data=data_cycle_sc, columns=['Time'])
data_cycle_train_sc = cycle_sc.transform(data_cycle_train)
data_cycle_train_sc_df = pd.DataFrame(data=data_cycle_train_sc, columns=['Time'])
data_cycle_test_sc = cycle_sc.transform(data_cycle_test)
data_cycle_test_sc_df = pd.DataFrame(data=data_cycle_test_sc , columns=['Time'])

data_HI_sc = HI_sc.fit_transform(data_HI)
data_HI_train_sc = HI_sc.transform(data_HI_train)
data_HI_train_sc_df = pd.DataFrame(data=data_HI_train_sc, columns=['Fuesed_HI'])

data_HI_test_sc = HI_sc.transform(data_HI_test)
data_HI_test_sc_df = pd.DataFrame(data=data_HI_test_sc, columns=['Fuesed_HI'])

data_TV_sc = TV_sc.fit_transform(data_SOH)
data_TV_train_sc = TV_sc.transform(data_SOH_train)
data_TV_train_sc_df = pd.DataFrame(data=data_TV_train_sc, columns=['TV'])

data_TV_test_sc = TV_sc.transform(data_SOH_test)
data_TV_test_sc_df = pd.DataFrame(data=data_TV_test_sc, columns=['TV'])

for i in range(lookback):
    data_TV_train_sc_df ['TV'+str(i+1)] = data_TV_train_sc_df ['TV'].shift(-i-1)

train_window = pd.concat([data_HI_train_sc_df[lookback:].reset_index(drop=True),
                            data_TV_train_sc_df], axis=1,ignore_index=True)

train_data = np.array(train_window)[:-lookback]
X_train = train_data[:,0:-1]
y_train = train_data[:, -1:]

  ###test data ####
for i in range(lookback):
    data_TV_test_sc_df['TV'+str(i+1)] = data_TV_test_sc_df['TV'].shift(-i-1)

test_window = pd.concat([data_HI_test_sc_df[lookback:].reset_index(drop=True),
                            data_TV_test_sc_df], axis=1,ignore_index=True)

test_data = np.array(test_window)[:-lookback]
X_test = test_data[:,0:-1]
y_test = test_data[:, -1:]


def bilstm_att_TR(neurons_1, neurons_2, dropout, n_output,pre_model=pre_model,freeze=True,verbose=True):
    inputs = tf.keras.Input(shape=(1,n_features))
    b1 = Bidirectional(LSTM(neurons_1, return_sequences=True))(inputs)
    b2 = Bidirectional(LSTM(neurons_2, return_sequences=True, activation='relu'))(b1)
    drop = Dropout(dropout)(b1)
    att = BahdanauAttention(return_sequences=False)(drop)
    outputs =  Dense(n_output)(att)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    if pre_model:
        for i in range(2, len(model.layers) - 1):
            model.layers[i].set_weights(pre_model.layers[i].get_weights())
            if freeze: 
                model.layers[i].trainable = False
        
    model.compile(optimizer=Adam(), loss='mse', metrics=['accuracy'])
    if verbose: print(model.summary())    
    
    return model

# def bilstm_TR(neurons_1, dropout, n_output,pre_model=pre_model1,freeze=True,verbose=True):
#     inputs = tf.keras.Input(shape=(1,n_features))
#     b1 = Bidirectional(LSTM(neurons_1,
#                         return_sequences=True))(inputs)
#     # b2 = Bidirectional(LSTM(neurons_2,
#     #                     return_sequences=True, activation='relu'))(b1)
#     drop = Dropout(dropout)(b1)
#     #att = BahdanauAttention(return_sequences=False)(drop)
#     outputs =  Dense(n_output)(drop)
#     model = tf.keras.Model(inputs=inputs, outputs=outputs)
#     if pre_model:
#         for i in range(2, len(model.layers) - 1):
#             model.layers[i].set_weights(pre_model.layers[i].get_weights())
#             if freeze: 
#                 model.layers[i].trainable = False
        
#     model.compile(optimizer=Adam(), loss='mse', metrics=['accuracy'])
#     if verbose: print(model.summary())    
    
#     return model

# def lstm_att_TR(neurons_1, dropout, n_output,pre_model=pre_model_lstm, freeze=True,verbose=True):
#     inputs = tf.keras.Input(shape=(1,n_features))
#     b1 = LSTM(neurons_1,
#                         return_sequences=True)(inputs)
#     # b2 = LSTM(neurons_2,
#     #                     return_sequences=True, activation='relu')(b1)
#     drop = Dropout(dropout)(b1)
#     att = BahdanauAttention(return_sequences=False)(drop)
#     outputs =  Dense(n_output)(att)
#     model = tf.keras.Model(inputs=inputs, outputs=outputs)
#     if pre_model:
#         for i in range(2, len(model.layers) - 1):
#             model.layers[i].set_weights(pre_model.layers[i].get_weights())
#             if freeze: 
#                 model.layers[i].trainable = False
        
#     model.compile(optimizer=Adam(), loss='mse', metrics=['accuracy'])
#     if verbose: print(model.summary())    
    
#     return model

# def lstm_TR(neurons_1, dropout, n_output,pre_model=pre_model_lstm1, freeze=True,verbose=True):
#     inputs = tf.keras.Input(shape=(1,n_features))
#     b1 = LSTM(neurons_1, return_sequences=True)(inputs)
#     # b2 = LSTM(neurons_2,
#     #                     return_sequences=True, activation='relu')(b1)
#     drop = Dropout(dropout)(b1)
#     # att = BahdanauAttention(return_sequences=False)(drop)
#     outputs =  Dense(n_output)(drop)
#     model = tf.keras.Model(inputs=inputs, outputs=outputs)
#     if pre_model:
#         for i in range(2, len(model.layers) - 1):
#             model.layers[i].set_weights(pre_model.layers[i].get_weights())
#             if freeze: 
#                 model.layers[i].trainable = False
        
#     model.compile(optimizer=Adam(), loss='mse', metrics=['accuracy'])
#     if verbose: print(model.summary())    
    
#     return model

X_roll = X_train.copy()
y_roll = y_train.copy()
X_train = X_train.reshape(X_train.shape[0], 1, X_train.shape[1])

X_roll_test = X_test.copy()
y_roll_test = y_test.copy()
X_test = X_test.reshape(X_test.shape[0], 1, X_test.shape[1])

import time
start = time.time()
tf.random.set_seed(seed)
bilstm_att_TR_model=bilstm_att_TR(neurons_1,  neurons_2, dropout, n_output,pre_model=pre_model,freeze=True,verbose=True)
bilstm_att_history=compile_and_fit(bilstm_att_TR_model,X_train,y_train, batch_size, epochs=epochs)

end = time.time()
print(round((end-start),2),"seconds")

##y_pred_train_bilstm_att_TR = bilstm_att_TR_model.predict(X_train)
y_pred_test_bilstm_att_TR = bilstm_att_TR_model.predict(X_test)
y_pred_test_bilstm_att_TR = TV_sc.inverse_transform(y_pred_test_bilstm_att_TR)
pd.DataFrame(y_pred_test_bilstm_att_TR.reshape(-1, 1)).to_csv("with_TL_BiLSTM_Att_8_8.csv")
draw(y_pred_test_bilstm_att_TR,Y_test)




# ### Bi-LSTM_TL
# start1 = time.time()
# tf.random.set_seed(seed)
# bilstm_TR_model=bilstm_TR(neurons_1, dropout, n_output,pre_model=pre_model1,freeze=True,verbose=True)
# bilstm_history=compile_and_fit(bilstm_TR_model,X_train,y_train, batch_size, epochs=epochs)
# end1 = time.time()    


# #####test ###
# y_pred_test_bilstm_TR = bilstm_TR_model.predict(X_test)
# y_pred_test_bilstm_TR = TV_sc.inverse_transform(y_pred_test_bilstm_TR.reshape(-1,1))
# pd.DataFrame(y_pred_test_bilstm_TR.reshape(-1, 1)).to_csv("BiLSTM_with_tl_15.csv")
# draw(y_pred_test_bilstm_TR,Y_test )


# ### LSTM_Att_TL
# start2 = time.time()
# tf.random.set_seed(seed)
# lstm_TR_model=lstm_att_TR(neurons_1, dropout, n_output, pre_model=pre_model_lstm, freeze=True,verbose=True)
# lstm_att_history=compile_and_fit(lstm_TR_model,X_train,y_train, batch_size, epochs=epochs)
# end2 = time.time()    



# #########test#
# y_pred_test_lstm_TR = lstm_TR_model.predict(X_test)
# y_pred_test_lstm_TR = TV_sc.inverse_transform(y_pred_test_lstm_TR.reshape(-1,1))
# pd.DataFrame(y_pred_test_lstm_TR.reshape(-1, 1)).to_csv("LSTM_without_tl_200.csv")
# draw(y_pred_test_lstm_TR,Y_test )


# # ### LSTM_TL
# start2 = time.time()
# tf.random.set_seed(seed)
# lstm_TR_model1=lstm_TR(neurons_1, dropout, n_output, pre_model=pre_model_lstm1, freeze=True,verbose=True)
# lstm_att_history=compile_and_fit(lstm_TR_model1,X_train,y_train, batch_size, epochs=epochs)
# end2 = time.time()    



# #########test#
# y_pred_test_lstm_TR1 = lstm_TR_model1.predict(X_test)
# y_pred_test_lstm_TR1 = TV_sc.inverse_transform(y_pred_test_lstm.reshape(-1,1))
# pd.DataFrame(y_pred_test_lstm_TR1.reshape(-1, 1)).to_csv("LSTM.csv")
# draw(y_pred_test_lstm,Y_test )


      ### BiLSTM_Att_TL
result1+= '\nRunning Time BiLSTM_Att: {}'.format(end-start)
result += '\n\nBiLSTM_Att_MAE: {}'.format(MAE1(y_pred_test_bilstm_att, y_test))
result += '\nBiLSTM_Att_RMSE: {}'.format(RMSE1(y_pred_test_bilstm_att, y_test))
result += '\nBiLSTM_Att_MSE: {}'.format(MSE1(y_pred_test_bilstm_att, y_test))
 
#      #### BiLSTM_TL
# result1+= '\nRunning TimeBiLSTM: {}'.format(end1-start1)
# result += '\n\nBiLSTM_MAE: {}'.format(MAE1(y_test, y_pred_test_bilstm))
# result += '\nBiLSTM_RMSE: {}'.format(RMSE1(y_test, y_pred_test_bilstm)
# result += '\nBiLSTM_MSE: {}'.format(MSE1(y_test, y_pred_test_bilstm))
#

#     #### LSTM_Att_TL
# result1+= '\nRunning Time LSTM_Att: {}'.format(end2-start2)
# result += '\n\nLSTM_Att_MAE: {}'.format(MAE1(y_pred_test_lstm_am, y_test))
# result += '\nLSTM_Att_RMSE: {}'.format(RMSE1(y_pred_test_lstm_am, y_test))
# result += '\nLSTM_Att_MSE: {}'.format(MSE1(y_pred_test_lstm_am, y_test))

#     #### LSTM_TL
# result1+= '\nRunning Time LSTM: {}'.format(end2-start2)
# result += '\n\nLSTM_MAE: {}'.format(MAE1(y_pred_test_lstm, y_test))
# result += '\nLSTM_RMSE: {}'.format(RMSE1(y_pred_test_lstm, y_test))
# result += '\nLSTM_MSE: {}'.format(MSE1(y_pred_test_lstm, y_test))

print(result1)
print(result)