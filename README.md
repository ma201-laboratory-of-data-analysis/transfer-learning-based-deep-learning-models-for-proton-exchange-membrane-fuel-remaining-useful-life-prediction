# [paper title] Transfer learning-based deep learning models for proton exchange membrane fuel remaining useful life prediction

## Introduction
This repository comprised the experiment dataset (FC1 and FC2), offline training, and online RUL prediction processes. Three crucial parts are provided in the offline model training folder: the process of VAE feature fusion, Knee-onset and Knee point, and Source model training. After training the deep learning (DL) model, the online RUL prediction process is used to get the prediction in a folder of online predictions.
