
from sklearn import metrics
import pandas as pd
import numpy as np
import math
from sklearn.metrics import mean_squared_error #均方误差       MSE
from sklearn.metrics import mean_absolute_error #平方绝对误差  MAE
from sklearn.metrics import r2_score#R square #调用
import tensorflow as tf

# mape = np.mean(np.abs((y_true - y_pred) / y_true)) * 100
def MAPE(true,predict):
    L1=int(true.shape[0])
    L2=int(predict.shape[0])
    #print(L1,L2)
    if L1==L2:
        #SUM1=sum(abs(true-predict)/abs(true))
        SUM=0.0
        for i in range(L1-1):
            SUM=(abs(true[i,0]-predict[i,0])/true[i,0])+SUM
        per_SUM=SUM
        mape=per_SUM/L1
        return mape
    else:
        print("error")


def MAPE1(true,predict):
#    L1=int(true.shape[0])
#    L2=int(predict.shape[0])
    L1 = int(len(true))
    L2 = int(len(predict))
    #print(L1,L2)
    # true[true == 0.0] = np.nan  # avoid divided zero
    if L1==L2:
        #SUM1=sum(abs(true-predict)/abs(true))
        SUM=0.0
        for i in range(L1):
            #SUM=(abs(true[i,0]-predict[i,0])/true[i,0])+SUM
            SUM = abs((true[i]-predict[i])/true[i])+SUM
        per_SUM=SUM
        mape=per_SUM/L1
        return mape
    else:
        print("error")

def MSE(true_data, predict_data):
    testY = true_data
    testPredict = predict_data
    #mse = np.sum((predict_data-true_data)**2)/len(true_data) #跟数学公式一样的
    mse = mean_squared_error(testY[:,0], testPredict[:, 0])
    return mse

def MSE1(true_data, predict_data):
    testY = true_data
    testPredict = predict_data
    mse = mean_squared_error(testY[:], testPredict[:])
    return mse


def RMSE(true_data, predict_data):
    testY = true_data
    testPredict = predict_data
    # rmse = mse ** 0.5
    rmse = math.sqrt( mean_squared_error(testY[:,0], testPredict[:, 0]))
    return rmse

def RMSE1(true_data, predict_data):
    testY = true_data
    testPredict = predict_data
    rmse = math.sqrt( mean_squared_error(testY[:], testPredict[:]))
    return rmse


def MAE(true_data, predict_data):
    testY = true_data
    testPredict = predict_data
    #mae = np.sum(np.absolute(predict_data - true_data))/len(true_data)
    mae=mean_absolute_error(testY[:,0], testPredict[:, 0])
    return mae

def MAE1(true_data, predict_data):
    testY = true_data
    testPredict = predict_data
    mae=mean_absolute_error(testY[:], testPredict[:])
    return mae
def R2(true_data, predict_data):
    testY = true_data
    testPredict = predict_data
    r2=r2_score(testY[:], testPredict[:])
    return r2
def PPTS(true_data, predict_data,gamma):
    """ 
    Compute peak percentage threshold statistic
    args:
        y_true:the observed records
        y_pred:the predictions
        gamma:lower value percentage
    """
    # y_true
    r = pd.DataFrame(true_data,columns=['r'])
    # print('original time series:\n{}'.format(r))
    # predictions
    p = pd.DataFrame(predict_data,columns=['p'])
    # print('predicted time series:\n{}'.format(p))
    # The number of samples
    N = r['r'].size
    print('series size={}'.format(N))
    # The number of top data
    G = round((gamma/100)*N)
    rp = pd.concat([r,p],axis=1)
    rps=rp.sort_values(by=['r'],ascending=False)
    rps_g=rps.iloc[:G]
    y_true = (rps_g['r']).values
    y_pred = (rps_g['p']).values
    abss=np.abs((y_true-y_pred)/y_true*100)
    #print('abs error={}'.format(abss))
    sums = np.sum(abss)
    #print('sum of abs error={}'.format(abss))
    ppts = sums*(1/((100-gamma)*N))
    print('ppts('+str(gamma)+'%)={}'.format(ppts))
    return ppts


# MASE implemented courtesy of sktime - https://github.com/alan-turing-institute/sktime/blob/ee7a06843a44f4aaec7582d847e36073a9ab0566/sktime/performance_metrics/forecasting/_functions.py#L16
def mean_absolute_scaled_error(y_true, y_pred):
  """
  Implement MASE (assuming no seasonality of data).
  """
  mae = tf.reduce_mean(tf.abs(y_true - y_pred))

  # Find MAE of naive forecast (no seasonality)
  mae_naive_no_season = tf.reduce_mean(tf.abs(y_true[1:] - y_true[:-1])) # our seasonality is 1 day (hence the shifting of 1 day)

  return mae / mae_naive_no_season

def evaluate_preds(y_true, y_pred):
  # Make sure float32 (for metric calculations)
  y_true = tf.cast(y_true, dtype=tf.float32)
  y_pred = tf.cast(y_pred, dtype=tf.float32)

  # Calculate various metrics
  mae = tf.keras.metrics.mean_absolute_error(y_true, y_pred)
  mse = tf.keras.metrics.mean_squared_error(y_true, y_pred) # puts and emphasis on outliers (all errors get squared)
  rmse = tf.sqrt(mse)
  mape = tf.keras.metrics.mean_absolute_percentage_error(y_true, y_pred)
  mase = mean_absolute_scaled_error(y_true, y_pred)
  
  return {"mae": mae.numpy(),
          "mse": mse.numpy(),
          "rmse": rmse.numpy(),
          "mape": mape.numpy(),
          "mase": mase.numpy()}

def MASE(y_true, y_pred):
  # Make sure float32 (for metric calculations)
  y_true = tf.cast(y_true, dtype=tf.float32)
  y_pred = tf.cast(y_pred, dtype=tf.float32)

  # Calculate various metrics
 
  mase = mean_absolute_scaled_error(y_true, y_pred)
  
  return mase


def main():
    a = [1,2,3]
    b = [2,3,4]
    print(PPTS(b,a,5))
    print(MAPE1(b,a))
    print(MAE1(b,a))
    print(RMSE1(b,a))



if __name__=="__main__":
    main()










    