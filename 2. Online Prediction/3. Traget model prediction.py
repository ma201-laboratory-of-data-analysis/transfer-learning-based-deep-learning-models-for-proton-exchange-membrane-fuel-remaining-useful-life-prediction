import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import tensorflow as tf
import random, os, time, copy
from tensorflow.keras.layers import LSTM, Dense, Bidirectional, Dropout
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from tensorflow.keras.optimizers import Adam
from evaluate_data import *
from tensorflow.keras.layers import *
from tensorflow.keras.models import *
from tensorflow.keras import backend as K
from tensorflow.keras.models import load_model
plt.rcParams["font.family"]= "Times New Roman"
seed= 123
np.random.seed(seed)
random.seed(seed)
tf.random.set_seed(seed)

# This is the most basic attention that based on Bahdanau et al(2015)
class BahdanauAttention(Layer):
    def __init__(self, return_sequences=True):
        self.return_sequences = return_sequences
        super(BahdanauAttention,self).__init__()
    def build(self, input_shape):
        self.W=self.add_weight(name="att_weight", shape=(input_shape[-1],1),
                               initializer="normal")
        self.b=self.add_weight(name="att_bias", shape=(input_shape[1],1),
                               initializer="zeros")
        super(BahdanauAttention,self).build(input_shape)
    def call(self, x):
        e = K.tanh(K.dot(x,self.W)+self.b)
        a = K.softmax(e, axis=1)
        output = x*a
        if self.return_sequences:
            return output
        return K.sum(output, axis=1)
    def compute_output_shape(self,input_shape):
        return (input_shape[0],input_shape[-1])
    def get_config(self):
        return super(BahdanauAttention,self).get_config()

def loss(x,y): # x: predicted value, y:actual
    mse = np.sum((x-y)**2)/len(y)
    rmse = mse ** 0.5
    mae = np.sum(np.absolute(x-y))/len(y)
    mape = np.sum(np.absolute(x-y)/y)/len(y)*100
    return mse,rmse,mae, mape

def draw(preds,true):
    plt.figure(figsize=(7, 5))
    plt.plot(preds,color='royalblue')
    # plt.plot(preds1,color='green')
    plt.plot(true,color='crimson')
    plt.legend(['prediction','actual'], fontsize=15)
    plt.ylabel('Total Voltage (volt)', fontsize=15)
    plt.xlabel('Time (h)', fontsize=15)
    plt.title('Predicted Vs Actual Voltage plot', fontsize=15)
    plt.show()
    
def draw2(preds,preds1,true):
    plt.figure(figsize=(7, 5))
    plt.plot(preds,color='royalblue')
    plt.plot(preds1,color='green')
    plt.plot(true,color='crimson')
    plt.legend(['prediction','actual'], fontsize=15)
    plt.axhline(y=0.8, color='rosybrown', linestyle='-')
    plt.ylabel('SOH', fontsize=15)
    plt.xlabel('Cycle', fontsize=15)
    plt.title('Predicted Vs Actual SOH plot for test data', fontsize=15)
    plt.show()
    
def compile_and_fit(model, X_train, y_train, batch_size, epochs=20):
    model.compile(loss=tf.losses.MeanSquaredError(), optimizer=tf.optimizers.Adam())
    history= model.fit(X_train, 
                       y_train, 
                       epochs=epochs,
                       batch_size=batch_size,
                       verbose=0)
    return history
       
def bilstm_att(neurons_1,  dropout, n_output,pre_model=None,freeze=False,verbose=True):
    inputs = tf.keras.Input(shape=(1,n_features))
    b1 = Bidirectional(LSTM(neurons_1,
                        return_sequences=True))(inputs)
    drop = Dropout(dropout)(b1)
    att = BahdanauAttention(return_sequences=False)(drop)
    outputs =  Dense(n_output)(att)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    if pre_model:
        for i in range(2, len(model.layers) - 1):
            model.layers[i].set_weights(pre_model.layers[i].get_weights())
            if freeze: 
                model.layers[i].trainable = False
    model.compile(optimizer=Adam(), loss='mse', metrics=['accuracy'])
    if verbose: print(model.summary())    
    
    return model


if __name__== "__main__":
    file_path= os.path.join(os.getcwd(), "Source_Model_Input_Data_FC1.csv")
    battery= pd.read_csv(file_path)
    battery= battery.dropna()
    ### ts=1385; sp=1885; eol=2235

    ts=334 

    ### TV_eol= 3.345-3.345*0.04
    ###battery= battery.loc[(battery['Time'] >=ts)]
    ###battery= battery.iloc[: np.where(np.array(battery["TV"])<= TV_eol)[0][0], :]
###Optimal hyperparameters for training source model on online stack FC RUL predictions
    n_output= 1
    epochs= 590
    lookback=10
    n_features= 1+ lookback
    # neurons_2=134
    neurons_1= 134
    batch_size= 17
    dropout= 0.00098
    
    data= battery.loc[:,['Time','TV', 'Fuesed_HI']]
    data_cycle= pd.DataFrame(data['Time'])
    data_cycle_train= data_cycle[ts:]
    data_HI= pd.DataFrame(data['Fuesed_HI'])
    data_HI_train= data_HI[ts:]
    data_SOH= pd.DataFrame(data['TV'])
    data_SOH_train= data_SOH[ts: ]

    cycle_sc, HI_sc, TV_sc= MinMaxScaler(), MinMaxScaler(), MinMaxScaler()
    data_cycle_sc= cycle_sc.fit_transform(data_cycle)
    data_cycle_sc_df= pd.DataFrame(data=data_cycle_sc, columns=['Time'])
    data_cycle_train_sc= cycle_sc.transform(data_cycle_train)
    data_cycle_train_sc_df= pd.DataFrame(data=data_cycle_train_sc, columns=['Time'])
    data_HI_sc = HI_sc.fit_transform(data_HI)
    data_HI_train_sc = HI_sc.transform(data_HI_train)
    data_HI_train_sc_df = pd.DataFrame(data=data_HI_train_sc, columns=['Fuesed_HI'])
    data_TV_sc = TV_sc.fit_transform(data_SOH)
    data_TV_train_sc = TV_sc.transform(data_SOH_train)
    data_TV_train_sc_df = pd.DataFrame(data=data_TV_train_sc, columns=['TV'])
    
    for i in range(lookback):
        data_TV_train_sc_df ['TV'+ str(i+1)]= data_TV_train_sc_df ['TV'].shift(-i- 1)
        
    train_window= pd.concat([data_HI_train_sc_df[lookback:].reset_index(drop= True),
                             data_TV_train_sc_df],
                             axis= 1,
                             ignore_index= True)
    
    train_data= np.array(train_window)[: -lookback]
    
    X_train= train_data[:, 0: -1]
    y_train= train_data[:, -1: ]
    
    X_roll= X_train.copy()
    y_roll= y_train.copy()
    X_train= X_train.reshape(X_train.shape[0], 1, X_train.shape[1])

    global result
    global result1
    result='\nEvaluation.'
    result1= 'Computation Time'
    
    start= time.time()
    tf.random.set_seed(seed)
    bilstm_att_model= bilstm_att(neurons_1,  dropout, n_output)
    bilstm_att_history= compile_and_fit(bilstm_att_model,X_train,y_train, batch_size, epochs=epochs)
    end= time.time()
    print(round((end-start), 2),"seconds")
    y_pred_train_bilstm_att= bilstm_att_model.predict(X_train)
    

    
    ### transfer learning
    
    #1. BiLSTM_Att_TR
    bilstm_att_model.save('bilstm_att_model_FC1')
    pre_model=load_model('bilstm_att_model_FC1')
    
    
    ### online rul prediction for target domain (only for one step-ahead prediction)
    
    file_path= os.path.join(os.getcwd(), "Target_Model_Input_Data_FC2.csv")
    battery= pd.read_csv(file_path)
    battery= battery.dropna()
    original_battery= copy.copy(battery)
    ts= 256
    sp= 802 
    mote_carlo_simulation_times= 100

    TV_eol=3.33- 3.33* 0.06
    # TV_eol = 3.151

    battery= battery.loc[(battery["Time"]>= ts)]
    battery= battery.loc[(battery["Time"]< sp)]
    
    eol_H= np.where(np.array(original_battery["TV"]) <=TV_eol)[0][-1]
    #eol_H= 905 #925#947#
    
    plt.figure(figsize= (6, 3))
    plt.plot(original_battery["TV"], color= "black", label= "Original stack voltage")
    plt.plot(battery["TV"], color= "red", label= "target domain training data")
    plt.plot(original_battery["TV"].iloc[sp: ], color= "blue", label= "target domain unknown data")
    plt.scatter(original_battery["Time"].iloc[sp], original_battery["TV"].iloc[sp], color= "darkblue", s= 50, label= "Sp")
    plt.vlines(sp, np.max(original_battery["TV"]), np.min(original_battery["TV"]), color= "darkgreen", ls= "--")
    plt.hlines(TV_eol, np.max(original_battery["Time"]), np.min(original_battery["Time"]), color= "darkred", ls= "--", label= "EOL TH")
    plt.grid(True)
    plt.xticks(fontsize= 15)
    plt.yticks(fontsize= 15)
    plt.ylim(np.min(original_battery["TV"]), np.max(original_battery["TV"]))
    plt.xlabel("Time (h)", fontsize= 15)
    plt.ylabel("Stack Voltage", fontsize= 15)
    plt.legend(loc= "best")
    plt.show()

    normalizer_HI, normalizer_TV= MinMaxScaler(), MinMaxScaler()
    target_train_HI= normalizer_HI.fit_transform(np.array(battery["Fuesed_HI"]).reshape(-1, 1)).ravel()
    target_train_HI= pd.DataFrame({"HI": target_train_HI})
    target_train_TV= normalizer_TV.fit_transform(np.array(battery["TV"]).reshape(-1, 1)).ravel()
    target_train_TV_with_look_back= pd.DataFrame({"TV": target_train_TV})

    for i in range(lookback):
        target_train_TV_with_look_back["TV"+ str(i+ 1)]= target_train_TV_with_look_back["TV"].shift(-i- 1)
        
    target_train_window= pd.concat([target_train_HI[lookback: ].reset_index(drop= True),
                                    target_train_TV_with_look_back],
                                    axis= 1,
                                    ignore_index= True)
    target_train_data= np.array(target_train_window)[: -lookback]

    x_train, y_train= target_train_data[:, 0: -1], target_train_data[:, -1: ]
    x_train= x_train.reshape(x_train.shape[0], 1, x_train.shape[1])

    pre_bilstm_att_history= compile_and_fit(pre_model, x_train, y_train, batch_size, epochs= epochs)
    
    model_future_predictions= []; max_prediction_iteration= eol_H- battery["Time"].iloc[-1]; prediction_steps= 1
    ### the first column is HI and the rest columns are Vtotal components.
    init_step_model_input= x_train.reshape(-1, lookback+ 1)[-1, :]
    
    init_step_y_pred= bilstm_att_model.predict(init_step_model_input.reshape(1, 1, x_train.shape[2]))
    init_step_y_pred= normalizer_TV.inverse_transform(np.array(init_step_y_pred).reshape(-1, 1)).ravel()
    model_future_predictions+= [init_step_y_pred[0]]
    
    next_step_model_input= copy.copy(init_step_model_input)
    index= [sp, sp+ 1]
    for i in range(1, max_prediction_iteration+ 1):
        next_HI= normalizer_HI.transform(np.array(original_battery[index[0]: index[1]]["Fuesed_HI"]).reshape(-1, 1)).ravel()
        next_Vtotal= normalizer_TV.transform(np.array(original_battery[index[0]: index[1]]["TV"]).reshape(-1, 1)).ravel()
        
        next_step_model_input= np.insert(np.delete(next_step_model_input, 0), 0, next_HI)
        next_step_model_input= np.append(np.delete(next_step_model_input, 1), next_Vtotal)
        
        next_step_y_pred= bilstm_att_model.predict(next_step_model_input.reshape(1, 1, x_train.shape[2]), verbose= 0)
        next_step_y_pred= normalizer_TV.inverse_transform(np.array(next_step_y_pred).reshape(-1, 1)).ravel()
        
        model_future_predictions+= [next_step_y_pred[0]]
        
        if len(np.where(np.array(model_future_predictions)<= TV_eol)[0]) != 0:
            model_future_predictions= np.array(model_future_predictions)
            model_future_predictions= model_future_predictions[: np.where(model_future_predictions<= TV_eol)[0][0]]
            print("pred RUL: %d"%(len(model_future_predictions)))
            print("actu RUL: %d"%(eol_H- battery["Time"].iloc[-1]))
            break
        else:
            index[0]+= 1
            index[1]+= 1
    fut_pred = pd.DataFrame(model_future_predictions)  
    # fut_pred.to_csv("future_rul_pred.csv")      
    plt.figure(figsize= (6, 3))
    plt.plot(original_battery["TV"], color= "black", label= "Original stack voltage")
    plt.plot(battery["TV"], color= "red", label= "target domain training data")
    plt.plot(original_battery["TV"].iloc[sp: ], color= "blue", label= "target domain unknown data")
    plt.plot(np.arange(sp, sp+ len(model_future_predictions)), model_future_predictions, color= "darkgreen", label= "online rolling prediction results")
    plt.scatter(original_battery["Time"].iloc[812], original_battery["TV"].iloc[812], color= "darkblue", s= 50, label= "Sp")
    plt.vlines(812, np.max(original_battery["TV"]), np.min(original_battery["TV"]), color= "darkgreen", ls= "--")
    plt.hlines(TV_eol, np.max(original_battery["Time"]), np.min(original_battery["Time"]), color= "darkred", ls= "--", label= "EOL TH")
    plt.grid(True)
    plt.xticks(fontsize= 15)
    plt.yticks(fontsize= 15)
    plt.ylim(np.min(original_battery["TV"]), np.max(original_battery["TV"]))
    plt.xlabel("Time (h)", fontsize= 15)
    plt.ylabel("Stack Voltage", fontsize= 15)
    plt.legend(loc= "best", fontsize= 9)
    plt.show()
    
    print("predcition interval simulation ------------------------------------")
    pred_rul_ls= []; predcition_results= [];  prediction_SOH_mote_caro_dataframe= pd.DataFrame()
    for i in range(0, mote_carlo_simulation_times):
        seed= 124+ i
        np.random.seed(seed)
        random.seed(seed)
        tf.random.set_seed(seed)
       
        model_future_predictions= []; max_prediction_iteration= eol_H- battery["Time"].iloc[-1]; prediction_steps= 1
        
        ### the first column is HI and the rest columns are Vtotal components.
        init_step_model_input= x_train.reshape(-1, lookback+ 1)[-1, :]
        
        init_step_y_pred= bilstm_att_model.predict(init_step_model_input.reshape(1, 1, x_train.shape[2]), verbose= 0)
        init_step_y_pred= normalizer_TV.inverse_transform(np.array(init_step_y_pred).reshape(-1, 1)).ravel()
        model_future_predictions+= [init_step_y_pred[0]]
        
        next_step_model_input= copy.copy(init_step_model_input)
        index= [sp, sp+ 1]
        for i in range(1, max_prediction_iteration+ 1):
            next_HI= normalizer_HI.transform(np.array(original_battery[index[0]: index[1]]["Fuesed_HI"]).reshape(-1, 1)).ravel()
            next_Vtotal= normalizer_TV.transform(np.array(original_battery[index[0]: index[1]]["TV"]).reshape(-1, 1)).ravel()
                
            next_step_model_input= np.insert(np.delete(next_step_model_input, 0), 0, next_HI)
            next_step_model_input= np.append(np.delete(next_step_model_input, 1), next_Vtotal)
            
            next_step_y_pred= bilstm_att_model.predict(next_step_model_input.reshape(1, 1, x_train.shape[2]), verbose= 0)
            next_step_y_pred= normalizer_TV.inverse_transform(np.array(next_step_y_pred).reshape(-1, 1)).ravel()
            
            model_future_predictions+= [next_step_y_pred[0]]
            
            if len(np.where(np.array(model_future_predictions)<= TV_eol)[0]) != 0:
                model_future_predictions= np.array(model_future_predictions)
                model_future_predictions= model_future_predictions[: np.where(model_future_predictions<= TV_eol)[0][0]]
                pred_rul_ls.append(len(model_future_predictions))
                predcition_results.append(model_future_predictions)
                prediction_SOH_mote_caro_dataframe["pred SOH_"+ str((i + 1))]= np.array(model_future_predictions)
                prediction_SOH_std= prediction_SOH_mote_caro_dataframe.std(axis= 0)
                prediction_SOH_uper_bound= np.array(model_future_predictions)+ (0.5* np.array(prediction_SOH_std))[: len(model_future_predictions)]
                prediction_SOH_lower_bound= np.array(model_future_predictions)- (0.5* np.array(prediction_SOH_std))[: len(model_future_predictions)]
                Prediction_Interval= pd.DataFrame()
                Prediction_Interval["Pred TV"]= model_future_predictions
                Prediction_Interval["Lower"]= prediction_SOH_lower_bound
                Prediction_Interval["Uper"]= prediction_SOH_uper_bound
                print(Prediction_Interval)
                # Prediction_Interval.to_csv("prediction_interval_tv_256_812_2.csv")
                print("iteration: %d// pred RUL: %d"%((i+ 1), len(model_future_predictions)))
                break
            else:
                index[0]+= 1
                index[1]+= 1
            

  
   
    print("prediction interval: [%f, %f]"%(np.mean(prediction_SOH_lower_bound)- 1.96* np.std(prediction_SOH_lower_bound), np.mean(prediction_SOH_uper_bound)+ 1.96* np.std(prediction_SOH_uper_bound)))
    
    print("prediction interval: [%f, %f]"%(np.mean(pred_rul_ls)- 1.96* np.std(pred_rul_ls), np.mean(pred_rul_ls)+ 1.96* np.std(pred_rul_ls)))
    
    plt.figure(figsize= (10, 4))
    plt.plot(original_battery["TV"], color= "black", label= "Original stack voltage")
    plt.plot(battery["TV"], color= "red", label= "target domain training data")
    plt.plot(original_battery["TV"].iloc[sp: ], color= "blue", label= "target domain unknown data")
    plt.plot(np.arange(sp, sp+ len(model_future_predictions)), model_future_predictions, color= "darkgreen", label= "online rolling prediction results")
    plt.plot(np.arange(sp, sp+ len(model_future_predictions)), prediction_SOH_lower_bound, color= "orange", label= "lower % PI")
    plt.plot(np.arange(sp, sp+ len(model_future_predictions)), prediction_SOH_uper_bound, color= "orange", label= "Lpper % PI")
    plt.scatter(original_battery["Time"].iloc[sp], original_battery["TV"].iloc[sp], color= "darkblue", s= 50, label= "Sp")
    plt.vlines(sp, np.max(original_battery["TV"]), np.min(original_battery["TV"]), color= "darkgreen", ls= "--")
    plt.hlines(TV_eol, np.max(original_battery["Time"]), np.min(original_battery["Time"]), color= "darkred", ls= "--", label= "EOL TH")
    plt.grid(True)
    plt.xticks(fontsize= 12)
    plt.yticks(fontsize= 12)
    plt.ylim(np.min(original_battery["TV"]), np.max(original_battery["TV"]))
    plt.xlabel("Time (h)", fontsize= 12)
    plt.ylabel("Stack Voltage", fontsize= 12)
    plt.legend(loc= "best", fontsize= 9)
    plt.show()
    
    plt.figure(figsize= (6, 3))
    plt.plot(original_battery["TV"], color= "black", label= "Vtotal")
    plt.plot(battery["TV"], color= "red", label= "target domain training data")
    plt.plot(original_battery["TV"].iloc[sp: ], color= "blue", label= "target domain unknown data")
    
    for i in range(0, len(predcition_results)):
        if i== 0:
            plt.plot(np.arange(sp, sp+ len(predcition_results[i])), predcition_results[i], color= "darkgreen", label= "online rolling prediction results", alpha= 0.5)
        else:
            plt.plot(np.arange(sp, sp+ len(predcition_results[i])), predcition_results[i], color= "darkgreen", alpha= 0.5)
    plt.scatter(original_battery["Time"].iloc[sp], original_battery["TV"].iloc[sp], color= "darkblue", s= 50, label= "Sp")
    plt.vlines(sp, np.max(original_battery["TV"]), np.min(original_battery["TV"]), color= "darkgreen", ls= "--")
    plt.hlines(TV_eol, np.max(original_battery["Time"]), np.min(original_battery["Time"]), color= "darkred", ls= "--", label= "EOL TH")
    plt.grid(True)
    plt.xticks(fontsize= 15)
    plt.yticks(fontsize= 15)
    plt.ylim(np.min(original_battery["TV"]), np.max(original_battery["TV"]))
    plt.xlabel("Time (h)", fontsize= 15)
    plt.ylabel("Stack Voltage", fontsize= 15)
    plt.legend(loc= "best", fontsize= 9)
    plt.show()