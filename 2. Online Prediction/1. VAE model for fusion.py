# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 14:16:46 2023

@author: Administrator
"""
import random
import numpy as np
import torch
from torch import nn, optim
from torch import distributions
from sklearn.base import BaseEstimator, TransformerMixin
from torch.autograd import Variable
import os
import pandas as pd
import torch
import plotly
from torch.utils.data import DataLoader, TensorDataset
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
np. random.seed(123); random.seed(123); torch.manual_seed(123)

class Variational_Autoencoder_Dense(torch.nn.Module):
  def __init__(self, in_shape, hidden_size, latnet_dims):
     super(Variational_Autoencoder_Dense, self).__init__()
     self.in_shape= in_shape
     self.hidden_size= hidden_size
     self.latent_dims= latnet_dims
     
     self.encoder= nn.Sequential()
     self.encoder.add_module("Dense_01", nn.Linear(self.in_shape, self.hidden_size[0]))
     self.encoder.add_module("Sigmoid_01", nn.Sigmoid())
     self.encoder.add_module("Dense_02", nn.Linear(self.hidden_size[0], self.hidden_size[1]))
     self.encoder.add_module("Sigmoid_02", nn.Sigmoid())
     
     self.decoder= nn.Sequential()
     self.decoder.add_module("Dense_01", nn.Linear(self.hidden_size[1], self.hidden_size[0]))
     self.decoder.add_module("Sigmoid_01", nn.Sigmoid())
     self.decoder.add_module("Dense_02", nn.Linear(self.hidden_size[0], self.in_shape))
     self.decoder.add_module("Sigmoid_02", nn.Sigmoid())

     self.mean_vector_layer= nn.Sequential()
     self.mean_vector_layer.add_module("Mean_vector", nn.Linear(self.hidden_size[1], 
    self.latent_dims))
     self.log_var_vector_layer= nn.Sequential()
     self.log_var_vector_layer.add_module("Log_var_vector", nn.Linear(self.hidden_size[1], 
    self.latent_dims)) 
     self.latent_vector_layer= nn.Sequential()
     self.latent_vector_layer.add_module("Z_vector", nn.Linear(self.latent_dims, 
    self.hidden_size[1]))

  def reparameterization_trcik(self, h_x): 
     mean= self.mean_vector_layer(h_x)
     var= self.log_var_vector_layer(h_x)
     var= var.mul(1/ 2).exp_() 
     epsilon= torch.randn_like(mean)
     z= mean+ var* epsilon
     latent_z= self.latent_vector_layer(z) 
     return latent_z, z, mean, var
     
  def forward(self, x):
     encoded_x= self.encoder(x)
     latent_z, z, mean, log_var= self.reparameterization_trcik(encoded_x)
     decoded_x= self.decoder(latent_z)
     return decoded_x, latent_z, z, mean, log_var
 
if __name__== "__main__":
    dataset= pd.read_csv("C:/Users/USER/Desktop/MODEL_FIRST PAPER/4_future_fussion_VAE/VAE_Model_FC2/FC2_features_original.csv")
    
    dataset= dataset.drop(["Time","VT"], axis= 1)
    scaler= MinMaxScaler()
    dataset= scaler.fit_transform(dataset)
    
    train_x= torch.FloatTensor(np.array(dataset))
    
    ### model hyperparameters
    batch_size= 64
    epochs=700#8000#8500
    hidden_size= [16, 32]#32#64
    latnet_dims= 1
    learning_rate= 0.001
    
    tensorloader= TensorDataset(train_x, train_x)
    tensorloader= DataLoader(tensorloader, batch_size= batch_size, shuffle= True)
    
    Vae_model= Variational_Autoencoder_Dense(in_shape= train_x.shape[1], hidden_size= hidden_size, latnet_dims= latnet_dims)
    
    optimizer= torch.optim.Adam(Vae_model.parameters(), lr= learning_rate)
    loss_function= nn.MSELoss()
    
    for e in range(0, epochs):
        batch_loss= []
        for i, data in enumerate(tensorloader):
            optimizer.zero_grad()
            x, y= data
            decoded_x, latent_z, z, mean, log_var= Vae_model(x)
            loss= loss_function(decoded_x, y)
            batch_loss.append(loss.data.cpu().numpy())
            loss.backward()
            optimizer.step()
        if e% 20== 0:
            print("epoch: %4d// model loss: %2.6f"%(e, np.mean(batch_loss)))

    Vae_model.eval()
    decoded_x, latent_z, z, mean, log_var= Vae_model(train_x)
    z= z.data.cpu().numpy()
    
    SHOW_PLOT= True
    if SHOW_PLOT== True:
          plt.figure(num= 1, figsize= (8, 5))
          # plt.hist(z)
          plt.plot(z)
          plt.grid(True)
          # plt.xlabel("Fused Health Index", fontsize= 13)
          plt.xlabel("Time(h)", fontsize= 13)
          plt.ylabel("Fused Health Index", fontsize= 13)
          plt.show()
    
    # df_f_2= pd.DataFrame(z)
    # print("-------converting into csv file with a new file name------")
    # get4=df_f_2.to_csv("C:/Users/Administrator/Desktop/VAE_Model2/data/FC2/VAE_model/Fuesed2_17_7.csv")